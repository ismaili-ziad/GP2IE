FROM gcc:10.2
RUN ln -s /usr/bin/make /usr/bin/gmake && ln -s /usr/bin/make /usr/local/bin/gmake
RUN apt update && apt install -y bison flex libperl-dev libgtk2.0-dev libjudy-dev pandoc && rm -rf /var/lib/apt/lists/*
RUN git clone --single-branch --branch latest https://gitlab.com/ismaili-ziad/GP2E.git /tmp/gp2 && mv /tmp/gp2/Compiler /gp2 && rm -rf /tmp/gp2
COPY build.sh /gp2/
WORKDIR /gp2
RUN ./build.sh
RUN make install

FROM gcc:10.2
RUN apt update && apt install -y libjudy-dev && rm -rf /var/lib/apt/lists/*
COPY --from=0 /usr/local/bin/gp2 /usr/local/bin/gp2
COPY --from=0 /gp2/lib /gp2/lib
COPY gp2i.sh gp2c.sh run.sh /usr/local/bin/
RUN mkdir /data
ENTRYPOINT ["run.sh", "/data", "/tmp/out"]
