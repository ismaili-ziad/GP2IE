#!/bin/bash

if [[ -n "$5" ]]; then
  echo "UNKNOWN COMMAND."
  exit 1
fi

if [[ ! -n "$3" ]]; then
  echo "UNKNOWN COMMAND."
  exit 1
fi

rm -rf tests &> /dev/null || true

if [ $1 == "-q" ]; then
  git clone --single-branch --branch $2 https://gitlab.com/ismaili-ziad/E-tests tests &> /dev/null
elif [ $1 == "-v" ]; then
  git clone --single-branch --branch $2 https://gitlab.com/ismaili-ziad/E-tests tests
else
  git clone --single-branch --branch $1 https://gitlab.com/ismaili-ziad/E-tests tests
fi

cd tests
if [[ -n "$4" ]]; then
  bash test.sh $1 $3 $4
else
  bash test.sh $2 $3
fi
