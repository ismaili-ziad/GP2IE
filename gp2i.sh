#!/bin/bash

cd $2

if ./gp2run $1/$3 &> $1/gp2run.log; then
  if grep -q "Error: " $1/gp2run.log || grep -q "Error at " $1/gp2run.log || grep -q "Error parsing " $1/gp2run.log; then
    >&2 sed '/^$/d' $1/gp2run.log
    exit 2
  fi

  if [ ! -f gp2.output ]; then
    >&2 echo "Error: No output graph was written!"
    >&2 sed '/^$/d' $1/gp2run.log
    exit 2
  fi

  if grep -q "No output graph: " gp2.output; then
    >&2 echo "Error: No output graph was written!"
    >&2 sed '/^$/d' gp2.output
    exit 2
  fi

  cat gp2.output
else
  >&2 sed '/^$/d' $1/gp2run.log
  exit 2
fi
