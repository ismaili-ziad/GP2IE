# GP2I

## Prereqs

Any modern Linux or Mac running Docker CE 19.03 or greater will suffice.

## Running

Usage of the GP2I Docker image is easy. Simply mount the target directory and invoke GP2I.

For example, to execute a program, say `2colprog`, on graph `cycle-6`:

```bash
$ docker run -v ${PWD}:/data registry.gitlab.com/yorkcs/batman/gp2i:latest 2colprog cycle-6
```

This will write the result graph to stdout, or exit with an error.

### Configuration

It is also possible to set the `GP2_FLAGS` environment variable.

For example:

```bash
$ docker run -v ${PWD}:/data -e GP2_FLAGS='-f' registry.gitlab.com/yorkcs/batman/gp2i:latest 2colprog cycle-6
```

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2i:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2i:latest
```

## Tests

The test suite is split up into "quick" and "slow" tests.

```
$ ./test.sh [-q] [-v] master quick latest
```

```
$ ./test.sh [-q] [-v] master slow latest
```

`-q` is quiet mode, and `-v` is verbose.

Replace `latest` with the branch you'd like to run the tests against.
